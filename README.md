# FreeRTOS学习笔记

#### 介绍
无硬件学习FreeRTOS-学习笔记。

#### FreeRTOS模块学习概要

#### 1.消息队列

    应用场景：不定长数据的传输。
    
        消息发送方，可以是任务、中断；消息接收方，一般只能是任务。
    
    数据传输方式：将数据拷贝到队列的存储空间。
    
    实例设计：
    
        1.shell程序的串口接收；
    
        2.一个任务发送消息，另一个任务接收打印消息。
    
        3.一个任务发送消息，包括发送紧急消息，另一个任务接收打印消息。
    
            参考osMessageQueuePut()函数，自己实现一个紧急消息发送函数，把消息发送到队列头部。

#### 2.信号量

    使用场景：任务同步。
    
        可以是任务之间的同步，或者一个任务和一个(中断)事件的同步。它是一对一的同步机制。
    
    实例设计：
    
        1.定时中断+计时显示任务；
    
        2.定时任务+显示任务。

#### 3.互斥量

    使用场景：两个或两个以上的任务使用同一个资源时，起到互斥访问的作用。
    
        如果用信号量来实现，会导致优先级翻转问题。互斥量通过优先级继承的方法，解决了这个问题。
    
    实例设计：
    
        1.串口打印输出的使用；
    
        2.优先级翻转的演示例程，H任务+M任务+L任务。
    
            H任务和L任务使用同一个资源，先用信号量实现互斥访问，发现H等待L释放资源时没有得到运行，反而M任务得到运行。
    
            然后改用互斥量实现，解决上面的问题。
    
        3.在例2基础上修改：
        
        	H任务申请资源前，设置一个标志位，释放资源后，清零该标志；
    
    		L任务释放资源之前，打印L任务的优先级，以及当前H任务是否在申请资源；
    		
    		观察L任务的优先级变化。

#### 4.邮箱队列

    应用场景：每次传输固定大小的数据块。
    
        CMSIS-RTOS V2取消了邮箱队列的API。
    
        本质上邮箱队列是通过消息队列实现的。
    
        邮箱队列传输的是数据的指针。用户可以根据实际需要，自定义邮件的结构。
    
    注意：
    
        在邮件发送后未被读取之前，其指向的存储空间不应该被修改。
    
        所以，实现邮箱队列的时候，应该配合使用计数信号量对邮件进行管理。
    
    实例设计：
    
        1.通过消息队列实现邮箱队列功能；
    
        2.一个任务发送邮件，另一个任务接收显示邮件内容。

#### 5.软件定时器

    应用场景：硬件定时器不够用，且定时服务要求的精度不高。
    
        软件定时器本质上是任务。
    
    实例设计：软件定时器+计时显示任务。

#### 6.任务管理

    包括任务创建、挂起、恢复、删除、退出等操作。
    
    实例设计：
    
        创建三个任务A、B、C。
    
        任务A运行一段时间后，自己退出。
    
        任务B运行一段时间后，发送一个信号量，然后将自己挂起。
    
        任务C接收任务B发过来的信号量，然后恢复任务B运行。
        
        任务B再次发送信号量，然后再次将自己挂起。
        
        任务C再次接收任务B发过来的信号量，删除任务B运行，然后自己退出。

#### 7.事件

    应用场景：实现一对多，多对多的同步。
    
        一个任务可以等待多个事件的发生：可以是任意一个事件发生时唤醒任务进行事件处理；也可以是几个事件都发生后才唤醒任务进行事件处理。
    
        同样，也可以是多个任务同步多个事件。
    
    注意：事件机制不提供数据传输功能。
    
    实例设计：
    
        创建3个任务：任务A负责发送事件，任务B“或”的方式等待多个事件，任务C以“与”的方式等待多个事件。

#### 8.任务事件

    应用场景：可替代一对多的事件同步功能。
    
        优势是消耗资源少，效率高。
    
    实例设计：
    
        创建3个任务：任务A负责给任务B和C发送任务事件，任务B“或”的方式等待多个事件，任务C以“与”的方式等待多个事件。
