
#ifndef	__TINY_SHELL_H__
#define	__TINY_SHELL_H__

#include "stm32f1xx_hal.h"

#define		KEY_SPACE		0x20
#define		KEY_BACKSPACE	0x08
#define		KEY_ENTER		0x0D
#define		KEY_DELETE 		0x7F

#define	SHELL_LINE_MAX_LEN	256

typedef int (*T_ShellFun)(int argc, char*argv[]);

typedef struct
{
	char* name; 	/*命令的名字*/
	char* help; 	/*帮助描述*/
	T_ShellFun fun; /*命令函数*/
}T_ShellCmd;

void tiny_shell(uint8_t rxData);

#endif	// __TINY_SHELL_H__

