
#ifndef	__QUEUE_URGENT_H__
#define	__QUEUE_URGENT_H__

#include "cmsis_os2.h"                  // ::CMSIS:RTOS2

/// Put a urgent Message into a Queue or timeout if Queue is full.
/// \param[in]     mq_id         message queue ID obtained by \ref osMessageQueueNew.
/// \param[in]     msg_ptr       pointer to buffer with message to put into a queue.
/// \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue or 0 in case of no time-out.
/// \return status code that indicates the execution status of the function.
osStatus_t urgent_MessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout);

/// Put a urgent Message into a Queue in ISR context.
/// \param[in]     mq_id         message queue ID obtained by \ref osMessageQueueNew.
/// \param[in]     msg_ptr       pointer to buffer with message to put into a queue.
/// \return status code that indicates the execution status of the function.
osStatus_t urgent_MessageQueuePutISR (osMessageQueueId_t mq_id, const void *msg_ptr);

#endif	// __QUEUE_URGENT_H__

