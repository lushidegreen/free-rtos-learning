/**
  ******************************************************************************
  * @file    logger_debug.h
  * @version V0.01
  * @author  lushide
  * @date    2021-08-19
  * @brief   This is the header file for logger module.
  *
  * @details 该文件提供日志的函数映射等级设置宏定义开关
  * 
  * <h2><center>&copy; COPYRIGHT(c) 2021 lushidegreen </center></h2>
  *
  ******************************************************************************
  */

#ifndef _LOGGER_DEBUG_H_
#define _LOGGER_DEBUG_H_

#define   LOGGER_ENABLED        1

#define   LOG_LEVEL_DEBUG       0
#define   LOG_LEVEL_INFO        1
#define   LOG_LEVEL_WARN        0
#define   LOG_LEVEL_ERROR       1
#define   LOG_LEVEL_FATAL       0

#if LOGGER_ENABLED


    #if LOG_LEVEL_DEBUG
        #define LOG_PRINT_DEBUG    printf("DEBUG:");printf
    #else
        #define LOG_PRINT_DEBUG(...)
    #endif // !LOG_LEVEL_DEBUG


    #if LOG_LEVEL_INFO
        #define LOG_PRINT_INFO   printf("INFO:");printf
    #else
        #define LOG_PRINT_INFO(...)
    #endif // !LOG_LEVEL_INFO


    #if LOG_LEVEL_WARN
        #define LOG_PRINT_WARN    printf("WARN:");printf
    #else
        #define LOG_PRINT_WARN(...)
    #endif // !LOG_LEVEL_WARN


    #if LOG_LEVEL_ERROR
        #define LOG_PRINT_ERROR   printf("ERROR:");printf
    #else
        #define LOG_PRINT_ERROR(...)
    #endif // !LOG_LEVEL_ERROR


    #if LOG_LEVEL_FATAL
        #define LOG_PRINT_FATAL    printf("FATAL:");printf
    #else
        #define LOG_PRINT_FATAL(...)
    #endif // !LOG_LEVEL_FATAL


#else

    #define LOG_PRINT_DEBUG(...)
    #define LOG_PRINT_INFO(...)
    #define LOG_PRINT_WARN(...)
    #define LOG_PRINT_ERROR(...)
    #define LOG_PRINT_FATAL(...)
    
#endif // !LOGGER_ENABLED

#endif // !_LOGGER_DEBUG_H_


