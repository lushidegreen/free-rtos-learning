
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for H_prioTask */
osThreadId_t H_prioTaskHandle;
const osThreadAttr_t H_prioTask_attributes = {
  .name = "H_prioTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for M_prioTask */
osThreadId_t M_prioTaskHandle;
const osThreadAttr_t M_prioTask_attributes = {
  .name = "M_prioTask",
  .priority = (osPriority_t) osPriorityBelowNormal1,
  .stack_size = 128 * 4
};

/* Definitions for L_prioTask */
osThreadId_t L_prioTaskHandle;
const osThreadAttr_t L_prioTask_attributes = {
  .name = "L_prioTask",
  .priority = (osPriority_t) osPriorityBelowNormal,
  .stack_size = 128 * 4
};

/* Definitions for Mutex02 */
osMutexId_t Mutex02Handle;
const osMutexAttr_t Mutex02_attributes = {
  .name = "Mutex02"
};

uint8_t H_Task_wait_flag = 0;

/**
  * @brief  Delay long time.
  * @param  argument: Not used
  * @retval None
  */
static void delay_long(uint16_t n)
{
	uint16_t i,j;
	
	for (i=0; i<n; i++) {
		for (j=0; j<30000; j++);
	}
}

/**
  * @brief  Function implementing the H_prioTask thread.
  * @param  argument: Not used
  * @retval None
  */
void H_prioTask(void *argument)
{
	uint16_t i;
	
	osDelay(500);
	
	for (i=0; i<3; i++) {
		printf("H_prioTask_wait_for_resource.\r\n");
		H_Task_wait_flag = 1;
		osMutexWait(Mutex02Handle, osWaitForever);
		printf("H_prioTask_is_using_resource.\r\n");
		delay_long(100);	/**< using resource for a while */
		printf("H_prioTask_release_resource.\r\n");
		osMutexRelease(Mutex02Handle);
		H_Task_wait_flag = 0;
		osDelay(500);
	}
	
	osDelay(100);
	printf("H_prioTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the M_prioTask thread.
  * @param  argument: Not used
  * @retval None
  */
void M_prioTask(void *argument)
{
	uint16_t i;
	
	osDelay(100);
	
	for (i=0; i<35; i++) {
		printf("  M_prioTask_is_runing.\r\n");
		osDelay(50);
	}
	
	osDelay(100);
	printf("  M_prioTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the L_prioTask thread.
  * @param  argument: Not used
  * @retval None
  */
void L_prioTask(void *argument)
{
	uint16_t i;
	osPriority_t priority;
	
	osDelay(100);
	
	for (i=0; i<6; i++)  {
		printf("    L_prioTask_wait_for_resource.\r\n");
		osMutexWait(Mutex02Handle, osWaitForever);
		printf("    L_prioTask_is_using_resource.\r\n");
		delay_long(100);	/**< using resource for a while */
		priority = osThreadGetPriority(L_prioTaskHandle);
		printf("    L_prioTask priority=%d, H_Task_wait_flag=%d.\r\n", priority, H_Task_wait_flag);
		printf("    L_prioTask_release_resource.\r\n");
		osMutexRelease(Mutex02Handle);
	}
	
	osDelay(100);
	printf("    L_prioTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个优先级不同的任务。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int mutex2CmdFunc(int argc, char*argv[])
{  
	/* Create the mutex(es) */
	Mutex02Handle = osMutexNew(&Mutex02_attributes);
	
	/* Create the thread(s) */	
	printf("create threads.\r\n");
	L_prioTaskHandle = osThreadNew(L_prioTask, NULL, &L_prioTask_attributes);
	M_prioTaskHandle = osThreadNew(M_prioTask, NULL, &M_prioTask_attributes);
	H_prioTaskHandle = osThreadNew(H_prioTask, NULL, &H_prioTask_attributes);
	
	printf("mutex2CmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd mutex2Cmd = 
{
	.name = "mutex2",
	.help = "Mutex Example2.",
	.fun = mutex2CmdFunc
};

