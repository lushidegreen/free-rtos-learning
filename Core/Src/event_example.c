
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for EwriteTask */
osThreadId_t EwriteTaskHandle;
const osThreadAttr_t EwriteTask_attributes = {
  .name = "EwriteTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for EreadTask1 */
osThreadId_t EreadTask1Handle;
const osThreadAttr_t EreadTask1_attributes = {
  .name = "EreadTask1",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for EreadTask2 */
osThreadId_t EreadTask2Handle;
const osThreadAttr_t EreadTask2_attributes = {
  .name = "EreadTask2",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for EvtFlags01 */
osEventFlagsId_t EvtFlags01Handle;
const osEventFlagsAttr_t EvtFlags01_attributes = {
  .name = "EvtFlags01"
};

/**
  * @brief  Function implementing the EwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void EwriteTask(void *argument)
{	
	osDelay(30);	/**< 延时,确保EreadTask1和EreadTask2都已经运行 */
	
	printf("Send Event 0x0001.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0001U);
	osDelay(100);
	
	printf("Send Event 0x0002.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0002U);
	osDelay(100);
	
	printf("Send Event 0x0004.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0004U);
	osDelay(100);
	
	printf("Send Event 0x0008.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0008U);
	
	osDelay(500);
	
	printf("Send Event 0x0010.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0010U);
	osDelay(100);
	
	printf("Send Event 0x0020.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0020U);
	osDelay(100);
	
	printf("Send Event 0x0040.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0040U);
	osDelay(100);
	
	printf("Send Event 0x0080.\r\n");
	osEventFlagsSet(EvtFlags01Handle, 0x0080U);
	osDelay(100);

	osDelay(100);
	printf("EwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the EreadTask1 thread.
  * @param  argument: Not used
  * @retval None
  */
void EreadTask1(void *argument)
{
	uint8_t i;
	uint32_t flags;
	
	for (i=0; i<4; i++) {
		flags = osEventFlagsWait(EvtFlags01Handle, 0x0001U | 0x0002U | 0x0004U | 0x0008U, osFlagsWaitAny, osWaitForever);
		printf("EreadTask1 receive 0x%X.\r\n", flags);
	}
	
	osDelay(100);
	printf("EreadTask1 exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the EreadTask2 thread.
  * @param  argument: Not used
  * @retval None
  */
void EreadTask2(void *argument)
{
	uint32_t flags;
	
	flags = osEventFlagsWait(EvtFlags01Handle, 0x0010U | 0x0020U | 0x0040U | 0x0080U, osFlagsWaitAll, osWaitForever);
	printf("EreadTask2 receive 0x%X.\r\n", flags);
	
	osDelay(100);
	printf("EreadTask2 exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int eventCmdFunc(int argc, char*argv[])
{
	/* Create the semaphores(s) */
	EvtFlags01Handle = osEventFlagsNew(&EvtFlags01_attributes);
  
	/* Create the thread(s) */
	printf("create EwriteTask.\r\n");
	EwriteTaskHandle = osThreadNew(EwriteTask, NULL, &EwriteTask_attributes);
	
	printf("create EreadTask1.\r\n");
	EreadTask1Handle = osThreadNew(EreadTask1, NULL, &EreadTask1_attributes);
	
	printf("create EreadTask2.\r\n");
	EreadTask2Handle = osThreadNew(EreadTask2, NULL, &EreadTask2_attributes);
	
	printf("eventCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd eventCmd = 
{
	.name = "event",
	.help = "Event Flags Example.",
	.fun = eventCmdFunc
};


