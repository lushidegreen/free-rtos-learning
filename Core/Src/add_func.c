
#include "stdio.h"
#include <stdlib.h>
#include "tiny_shell.h"

/**
  * @brief  add命令函数,实现两个整数的加法。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 返回两个整数的和.
  */
int addCmdFun(int argc, char*argv[])
{
	int sum = 0;
	int dat1=0,dat2 = 0;
	
	if (argc < 2) {
		printf("error: too few arguments in function call.\r\n");
	} else if (argc > 2) {
		printf("error: too many arguments in function call.\r\n");
	} else {
		dat1 = atoi(argv[0]);
		dat2 = atoi(argv[1]);
		sum = dat1 + dat2;
		printf("%d + %d = %d.\r\n", dat1, dat2, sum);
	}
	return sum;
}

/* add命令描述 */
T_ShellCmd addCmd = 
{
	.name = "add",
	.help = "add two integers.",
	.fun = addCmdFun
};


