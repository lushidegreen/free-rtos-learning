
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

extern TIM_HandleTypeDef htim4;
extern osSemaphoreId_t myBinSem01Handle;

/* Definitions for SwriteTask */
osThreadId_t SwriteTaskHandle;
const osThreadAttr_t SwriteTask_attributes = {
  .name = "SwriteTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for SreadTask */
osThreadId_t SreadTaskHandle;
const osThreadAttr_t SreadTask_attributes = {
  .name = "SreadTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for BinSem01 */
osSemaphoreId_t BinSem01Handle;
const osSemaphoreAttr_t BinSem01_attributes = {
  .name = "BinSem01"
};

/**
  * @brief  Function implementing the SwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void SwriteTask(void *argument)
{
	uint8_t i;
	
	osDelay(30);	/**< 延时,确保SwriteTask和SreadTask都已经运行 */
	
	/* 发送信号量 */
	for (i=0; i<10; i++) {
		osSemaphoreRelease(BinSem01Handle);	
		printf("send.\r\n");
		osDelay(100);
	}
	osDelay(100);
	printf("SwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the SreadTask thread.
  * @param  argument: Not used
  * @retval None
  */
void SreadTask(void *argument)
{
	uint8_t cnt = 0;
	
	/* 获取信号量 */
	while (1) {
		osSemaphoreAcquire(BinSem01Handle, osWaitForever);	
		printf("cnt=%d.\r\n", cnt);
		cnt++;
		if (10 == cnt) {
			break;
		}
	}
	osDelay(100);
	printf("SreadTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个任务,一个发送信号量,一个从队列中获取信号量。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int semCmdFunc(int argc, char*argv[])
{
	/* Create the semaphores(s) */
	BinSem01Handle = osSemaphoreNew(1, 0, &BinSem01_attributes);
  
	/* Create the thread(s) */
	printf("create SwriteTask.\r\n");
	SwriteTaskHandle = osThreadNew(SwriteTask, NULL, &SwriteTask_attributes);
	
	printf("create SreadTask.\r\n");
	SreadTaskHandle = osThreadNew(SreadTask, NULL, &SreadTask_attributes);
	
	printf("semCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd semCmd = 
{
	.name = "sem",
	.help = "Semaphore Example.",
	.fun = semCmdFunc
};


/**
  * @brief  Function implementing the S2readTask thread.
  * @param  argument: Not used
  * @retval None
  */
void S2readTask(void *argument)
{
	uint8_t cnt = 0;
	
	osDelay(100);
	
	printf("Start TIM4.\r\n");
	HAL_TIM_Base_Start_IT(&htim4);
	
	/* 获取信号量 */
	while (1) {
		osSemaphoreAcquire(myBinSem01Handle, osWaitForever);	
		printf("cnt=%d.\r\n", cnt);
		cnt++;
		if (10 == cnt) {
			break;
		}
	}
	
	printf("Stop TIM4.\r\n");
	HAL_TIM_Base_Stop_IT(&htim4);
	
	osDelay(100);
	printf("S2readTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建一个任务,从队列中获取信号量。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int sem2CmdFunc(int argc, char*argv[])
{
	/* Create the thread(s) */	
	printf("create S2readTask.\r\n");
	SreadTaskHandle = osThreadNew(S2readTask, NULL, &SreadTask_attributes);
	
	printf("sem2CmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd sem2Cmd = 
{
	.name = "sem2",
	.help = "Semaphore Example2.",
	.fun = sem2CmdFunc
};
