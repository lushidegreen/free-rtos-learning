
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for HprioTask */
osThreadId_t HprioTaskTaskHandle;
const osThreadAttr_t HprioTaskTask_attributes = {
  .name = "HprioTaskTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for LprioTaskTask */
osThreadId_t LprioTaskTaskHandle;
const osThreadAttr_t LprioTaskTask_attributes = {
  .name = "LprioTaskTask",
  .priority = (osPriority_t) osPriorityBelowNormal,
  .stack_size = 128 * 4
};

/* Definitions for Mutex01 */
osMutexId_t Mutex01Handle;
const osMutexAttr_t Mutex01_attributes = {
  .name = "Mutex01"
};

/**
  * @brief  user printf implement with osDelay.
  * @param  [in] str: string to be print
  * @retval None
  */
static void user_printf(const char *str)
{
	osMutexWait(Mutex01Handle, osWaitForever);
	while (0 != *str) {
		printf("%c", *str++);
		osDelay(1);
	}
	osMutexRelease(Mutex01Handle);
}

/**
  * @brief  Function implementing the HprioTaskTask thread.
  * @param  argument: Not used
  * @retval None
  */
void HprioTaskTask(void *argument)
{
	uint8_t i;

	for (i=0; i<3; i++) {
		user_printf("HprioTaskTask: Thanks for learning FreeRTOS.\r\n");
		osDelay(100);
	}
	osDelay(100);
	printf("HprioTaskTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the LprioTaskTask thread.
  * @param  argument: Not used
  * @retval None
  */
void LprioTaskTask(void *argument)
{
	uint8_t i;
	
	for (i=0; i<10; i++) {
		user_printf("  LprioTaskTask: Welcome to FreeRTOS world.\r\n");
		osDelay(30);
	}
	osDelay(100);
	printf("LprioTaskTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个优先级不同的任务。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int mutexCmdFunc(int argc, char*argv[])
{  
	/* Create the mutex(es) */
	Mutex01Handle = osMutexNew(&Mutex01_attributes);
	
	/* Create the thread(s) */	
	printf("create threads.\r\n");
	LprioTaskTaskHandle = osThreadNew(LprioTaskTask, NULL, &LprioTaskTask_attributes);
	HprioTaskTaskHandle = osThreadNew(HprioTaskTask, NULL, &HprioTaskTask_attributes);
	
	osDelay(1000);
	printf("mutexCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd mutexCmd = 
{
	.name = "mutex",
	.help = "Mutex Example.",
	.fun = mutexCmdFunc
};

