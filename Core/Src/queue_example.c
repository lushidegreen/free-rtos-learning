
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"
#include "queue_urgent.h"

/* Definitions for QwriteTask */
osThreadId_t QwriteTaskHandle;
const osThreadAttr_t QwriteTask_attributes = {
  .name = "QwriteTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for QreadTask */
osThreadId_t QreadTaskHandle;
const osThreadAttr_t QreadTask_attributes = {
  .name = "QreadTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for Queue01 */
osMessageQueueId_t Queue01Handle;
const osMessageQueueAttr_t Queue01_attributes = {
  .name = "Queue01"
};

/**
  * @brief  Function implementing the QwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void QwriteTask(void *argument)
{
	uint8_t send_dat[7] = {'A','B','C','D','E','F','G'};
	uint8_t i;
	
	osDelay(30);	/**< 延时,确保QwriteTask和QreadTask都已经运行 */
	
	/* 发送send_dat的值到消息队列中 */
	for (i=0; i<7; i++) {
		osMessageQueuePut(Queue01Handle, &send_dat[i], 0, 0);
		printf("[%c].\r\n", send_dat[i]);
		osDelay(30);
	}
	osDelay(100);
	printf("QwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the QreadTask thread.
  * @param  argument: Not used
  * @retval None
  */
void QreadTask(void *argument)
{
	uint8_t recv_dat;
	uint8_t cnt = 0;
	
	/* 从队列中获取数据 */
	for(cnt=0; cnt<7; cnt++) {
		osMessageQueueGet(Queue01Handle, &recv_dat, NULL, osWaitForever);	
		printf("%c.\r\n", recv_dat);
		osDelay(100);
	}
	osDelay(100);
	printf("QreadTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个任务,一个发送消息到队列,一个从队列中获取消息。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int queueCmdFunc(int argc, char*argv[])
{
	/* Create the queue(s) */
	Queue01Handle = osMessageQueueNew (16, sizeof(uint8_t), &Queue01_attributes);
  
	/* Create the thread(s) */
	printf("create QwriteTask.\r\n");
	QwriteTaskHandle = osThreadNew(QwriteTask, NULL, &QwriteTask_attributes);
	
	printf("create QreadTask.\r\n");
	QreadTaskHandle = osThreadNew(QreadTask, NULL, &QreadTask_attributes);
	
	printf("queueCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd queueCmd = 
{
	.name = "queue",
	.help = "Queue Example.",
	.fun = queueCmdFunc
};



/**
  * @brief  Function implementing the UrgQwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void UrgQwriteTask(void *argument)
{
	uint8_t send_dat = 'A';
	uint8_t i;
	
	osDelay(30);	/**< 延时,确保UrgQwriteTask和UrgQreadTask都已经运行 */
	
	/* 发送send_dat的值到消息队列中 */
	for (i=0; i<10; i++) {
		if (5 == i) {
			urgent_MessageQueuePut(Queue01Handle, &send_dat, 0);
			printf("<%c>.\r\n", send_dat);
		} else {
			osMessageQueuePut(Queue01Handle, &send_dat, 0, 0);
			printf("[%c].\r\n", send_dat);
		}
		send_dat++;
		osDelay(30);
	}
	
	send_dat = 'X';
	osMessageQueuePut(Queue01Handle, &send_dat, 0, 0);
	printf("[%c].\r\n", send_dat);
	
	osDelay(100);
	printf("UrgQwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the UrgQreadTask thread.
  * @param  argument: Not used
  * @retval None
  */
void UrgQreadTask(void *argument)
{
	uint8_t recv_dat;
	
	/* 从队列中获取数据 */
	while (1) {
		osMessageQueueGet(Queue01Handle, &recv_dat, NULL, osWaitForever);	
		printf("%c.\r\n", recv_dat);
		osDelay(100);
		if ('X' == recv_dat) {
			break;
		}
	}
	osDelay(100);
	printf("UrgQreadTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个任务,一个发送消息到队列,一个从队列中获取消息。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int queueUrgCmdFunc(int argc, char*argv[])
{
	/* Create the queue(s) */
	Queue01Handle = osMessageQueueNew (16, sizeof(uint8_t), &Queue01_attributes);
  
	/* Create the thread(s) */
	printf("create UrgQwriteTask.\r\n");
	QwriteTaskHandle = osThreadNew(UrgQwriteTask, NULL, &QwriteTask_attributes);
	
	printf("create UrgQreadTask.\r\n");
	QreadTaskHandle = osThreadNew(UrgQreadTask, NULL, &QreadTask_attributes);
	
	printf("queueUrgCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd queueUrgCmd = 
{
	.name = "queue_urg",
	.help = "Queue Urgent Example.",
	.fun = queueUrgCmdFunc
};

