
#include "stdio.h"
#include "string.h"
#include "tiny_shell.h"
#include "add_func.h"
#include "queue_example.h"
#include "sem_example.h"
#include "mutex_example.h"
#include "mutex2_example.h"
#include "mailbox_example.h"
#include "timer_example.h"
#include "task_example.h"
#include "event_example.h"
#include "task_evt_example.h"

char    shellLine[SHELL_LINE_MAX_LEN]   = {0};	/*用于存储从串口接收到的字符串*/
char    *shellParam[SHELL_LINE_MAX_LEN] = {0};	/*用于存储接收到的参数（包括命令名字）*/
int 	count = 0; /*用于记录输入的有效字符数量*/ 

extern UART_HandleTypeDef huart1;

int helpCmdFun(int argc, char*argv[]); 

/*help命令描述*/
T_ShellCmd helpCmd = 
{
	.name = "help",
	.help = "show all cmd list",
	.fun = helpCmdFun
};

/* shell命令列表 */
T_ShellCmd *sysCmd[] = 
{
	&helpCmd, /*只存放命令结构体的指针减少对存储的占用*/
	&addCmd,	/* 加法命令 */
	&queueCmd,	/* 队列例程 */
	&queueUrgCmd,	/* 队列例程 */
	&semCmd,	/* 信号量例程 */
	&sem2Cmd,	/* 信号量例程 */
	&mutexCmd,	/* 互斥量例程 */
	&mutex2Cmd,	/* 互斥量例程 */
	&mailboxCmd,	/* 邮箱例程 */
	&timerCmd,	/* 软定时器例程 */
	&taskCmd,	/* 任务管理例程 */
	&eventCmd,	/* 任务管理例程 */
	&task_evtCmd,	/* 任务管理例程 */
	NULL /*用于标记命令数组的结尾*/
};

/**
  * @brief  help命令函数,用于打印shell所支持的命令及其help信息。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 总是返回0.
  */
int helpCmdFun(int argc, char*argv[])
{
	uint8_t i;
	for(i=0; sysCmd[i]; i++)
	{
		printf("%-15s %s\r\n",sysCmd[i]->name, sysCmd[i]->help);
	}
	return 0;
}

/**
  * @brief  用于解析串口输入的命令，以回车键结束。
  * @param  [in] inputChar 串口输入的字符
  * @retval 收到回车或输入长度超限时,返回1,其他情况返回0.
  */
uint8_t shellGetOneLine(uint8_t inputChar)
{
	uint8_t uartData;
	
	if(count >= SHELL_LINE_MAX_LEN) /*长度超限*/
	{
		count = 0; /*清零计数器以便后续使用*/
		return 1;  /*返回有效标志*/
	}
	shellLine[count] = inputChar; /*记录数据*/
	
	if ( KEY_BACKSPACE == inputChar) /*退格键*/
	{	
		if(count>0)
		{
			count--; /*删除上一个接收到的字符*/
			
			printf("%c", inputChar);
			uartData = KEY_SPACE;
			printf("%c", uartData);
			uartData = KEY_BACKSPACE;
			printf("%c", uartData);
		}
	}
	
	else if ( KEY_ENTER == inputChar) /*接收到回车换行，证明已经收到一个完整的命令*/
	{
		shellLine[count] = '\0'; /*添加字符串结束符，刚好可以去掉'\r'或者'\n'*/
		count = 0; /*清零计数器以便后续使用*/
		
		printf("\r\n");
		
		return 1; /*返回有效标志*/
	}
	
	else if ((' ' <= inputChar)  && (inputChar <= '~'))	/* 可显示字符 */
	{
		count++;
		printf("%c", inputChar); /*把收到的字符输出到串口*/
	}
	
    return 0;
}

/**
  * @brief  从命令字符串中解析出命令及其参数。
  * @param  [in] line 命令行指针。
  * @param  [in] paramArry[] 命令、参数指针数组。
  * @param  [in] arryLen 指针数组paramArry[]的大小。
  * @retval ret 命令+参数的总个数。
  */
uint8_t shellGetParam(char* line, char *paramArry[], int arryLen)
{
	uint8_t i,ret;
	char *ptr = NULL;
	ptr = strtok(line, " ");
	for(i=0; ptr!=NULL &&i<arryLen; i++)
	{
		paramArry[i] = ptr;
		ptr = strtok(NULL, " ");		
	}
	ret = i;
	return ret;
}

/**
  * @brief  shell入口。
  * @param  [in] rxData,串口输入的字符
  * @retval None
  */
void tiny_shell(uint8_t rxData)
{
	uint8_t i;
	uint8_t paramNum;
	int value;
	
	if (shellGetOneLine(rxData)) {
		paramNum = shellGetParam(shellLine, shellParam, SHELL_LINE_MAX_LEN);
		if (paramNum) {
            for(i=0; sysCmd[i]; i++) /*查找与输入匹配的命令*/
			{            
                if(strcmp(sysCmd[i]->name, shellParam[0]) == 0) 
				{                
                    value = sysCmd[i]->fun(paramNum-1, &shellParam[1]); /*运行相应的命令函数*/
                    printf("value = %d\r\n", value); /*打印运行结果*/
					printf(">>>");
                    return;
                }
            }
            if(sysCmd[i] == NULL) /*没有找到命令*/ 
			{           
                printf("error: unknown symbol name \'%s\' \r\n",shellLine); /*打印错误信息*/
				printf(">>>");
            }
        }
	}
}

