
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for TCounterTask */
osThreadId_t TCounterTaskHandle;
const osThreadAttr_t TCounterTask_attributes = {
  .name = "TCounterTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for Timer01 */
osTimerId_t Timer01Handle;
const osTimerAttr_t Timer01_attributes = {
  .name = "Timer01"
};

/* Definitions for BinSem02 */
osSemaphoreId_t BinSem02Handle;
const osSemaphoreAttr_t BinSem02_attributes = {
  .name = "BinSem02"
};

/**
  * @brief  Function implementing the timer Callback01.
  * @param  argument: Not used
  * @retval None
  */
void timerCallback01(void *argument)
{
	osSemaphoreRelease(BinSem02Handle);
}


/**
  * @brief  Function implementing the TCounterTask thread.
  * @param  argument: Not used
  * @retval None
  */
void TCounterTask(void *argument)
{
	uint8_t cnt = 0;
	
	osDelay(100);
	
	printf("Start soft timer.\r\n");
	osTimerStart(Timer01Handle, 100);
	
	/* 获取信号量 */
	while (1) {
		osSemaphoreAcquire(BinSem02Handle, osWaitForever);	
		printf("cnt=%d.\r\n", cnt);
		cnt++;
		if (10 == cnt) {
			break;
		}
	}
	
	printf("Stop soft timer.\r\n");
	osTimerStop(Timer01Handle);
	
	osDelay(100);
	printf("TCounterTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建一个任务,启动/关闭软定时器,获取软定时器发送的信号量。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int timerCmdFunc(int argc, char*argv[])
{
	/* Create the timer(s) */
	Timer01Handle = osTimerNew(timerCallback01, osTimerPeriodic, NULL, &Timer01_attributes);
  
	/* Create the semaphores(s) */
	BinSem02Handle = osSemaphoreNew(1, 0, &BinSem02_attributes);
	
	/* Create the thread(s) */	
	printf("create TCounterTask.\r\n");
	TCounterTaskHandle = osThreadNew(TCounterTask, NULL, &TCounterTask_attributes);
	
	printf("timerCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd timerCmd = 
{
	.name = "timer",
	.help = "Soft Timer Example.",
	.fun = timerCmdFunc
};
