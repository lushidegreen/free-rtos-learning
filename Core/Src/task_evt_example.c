
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for TskEwriteTask */
osThreadId_t TskEwriteTaskHandle;
const osThreadAttr_t TskEwriteTask_attributes = {
  .name = "TskEwriteTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for TskEreadTask1 */
osThreadId_t TskEreadTask1Handle;
const osThreadAttr_t TskEreadTask1_attributes = {
  .name = "TskEreadTask1",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for TskEreadTask2 */
osThreadId_t TskEreadTask2Handle;
const osThreadAttr_t TskEreadTask2_attributes = {
  .name = "TskEreadTask2",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/**
  * @brief  Function implementing the TskEwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void TskEwriteTask(void *argument)
{	
	osDelay(30);	/**< 延时,确保TskEreadTask1和TskEreadTask2都已经运行 */
	
	printf("Send Event 0x0001.\r\n");
	osThreadFlagsSet(TskEreadTask1Handle, 0x0001U);
	osDelay(100);
	
	printf("Send Event 0x0002.\r\n");
	osThreadFlagsSet(TskEreadTask1Handle, 0x0002U);
	osDelay(100);
	
	printf("Send Event 0x0004.\r\n");
	osThreadFlagsSet(TskEreadTask1Handle, 0x0004U);
	osDelay(100);
	
	printf("Send Event 0x0008.\r\n");
	osThreadFlagsSet(TskEreadTask1Handle, 0x0008U);
	
	osDelay(500);
	
	printf("Send Event 0x0010.\r\n");
	osThreadFlagsSet(TskEreadTask2Handle, 0x0010U);
	osDelay(100);
	
	printf("Send Event 0x0020.\r\n");
	osThreadFlagsSet(TskEreadTask2Handle, 0x0020U);
	osDelay(100);
	
	printf("Send Event 0x0040.\r\n");
	osThreadFlagsSet(TskEreadTask2Handle, 0x0040U);
	osDelay(100);
	
	printf("Send Event 0x0080.\r\n");
	osThreadFlagsSet(TskEreadTask2Handle, 0x0080U);
	osDelay(100);

	osDelay(100);
	printf("TskEwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the TskEreadTask1 thread.
  * @param  argument: Not used
  * @retval None
  */
void TskEreadTask1(void *argument)
{
	uint8_t i;
	uint32_t flags;
	
	for (i=0; i<4; i++) {
		flags = osThreadFlagsWait(0x0001U | 0x0002U | 0x0004U | 0x0008U, osFlagsWaitAny, osWaitForever);
		printf("TskEreadTask1 receive 0x%X.\r\n", flags);
	}
	
	osDelay(100);
	printf("TskEreadTask1 exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the TskEreadTask2 thread.
  * @param  argument: Not used
  * @retval None
  */
void TskEreadTask2(void *argument)
{
	uint32_t flags;
	
	flags = osThreadFlagsWait(0x0010U | 0x0020U | 0x0040U | 0x0080U, osFlagsWaitAll, osWaitForever);
	printf("TskEreadTask2 receive 0x%X.\r\n", flags);
	
	osDelay(100);
	printf("TskEreadTask2 exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int task_evtCmdFunc(int argc, char*argv[])
{  
	/* Create the thread(s) */
	printf("create TskEwriteTask.\r\n");
	TskEwriteTaskHandle = osThreadNew(TskEwriteTask, NULL, &TskEwriteTask_attributes);
	
	printf("create TskEreadTask1.\r\n");
	TskEreadTask1Handle = osThreadNew(TskEreadTask1, NULL, &TskEreadTask1_attributes);
	
	printf("create TskEreadTask2.\r\n");
	TskEreadTask2Handle = osThreadNew(TskEreadTask2, NULL, &TskEreadTask2_attributes);
	
	printf("task_evtCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd task_evtCmd = 
{
	.name = "task_evt",
	.help = "Task Event Flags Example.",
	.fun = task_evtCmdFunc
};


