
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

#define		MAILBOX_LEN		16

/* Mail object structure */
typedef struct 
{
	uint32_t var1;
	uint32_t var2;
	uint8_t  var3;
}Mail_TypeDef;

Mail_TypeDef mail_box[MAILBOX_LEN];
 
/* Definitions for MwriteTask */
osThreadId_t MwriteTaskHandle;
const osThreadAttr_t MwriteTask_attributes = {
  .name = "MwriteTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for MreadTask */
osThreadId_t MreadTaskHandle;
const osThreadAttr_t MreadTask_attributes = {
  .name = "MreadTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for MailboxQ01 */
osMessageQueueId_t MailboxQ01Handle;
const osMessageQueueAttr_t MailboxQ01_attributes = {
  .name = "MailboxQ01"
};

/* Definitions for MailboxFreeQ */
osMessageQueueId_t MailboxFreeQHandle;
const osMessageQueueAttr_t MailboxFreeQ_attributes = {
  .name = "MailboxFreeQ"
};

/**
  * @brief  Init Mailbox queue.
  * @param  None
  * @retval None
  */
static void mailbox_init(void)
{
	Mail_TypeDef *pMail_msg;
	uint8_t i = 0;
	
	/* 发送邮箱的指针到空闲消息队列中 */
	for (i=0; i<MAILBOX_LEN; i++) {
		pMail_msg = &mail_box[i];
		osMessageQueuePut(MailboxFreeQHandle, &pMail_msg, 0, 0);
	}
}

/**
  * @brief  Function implementing the MwriteTask thread.
  * @param  argument: Not used
  * @retval None
  */
void MwriteTask(void *argument)
{
	Mail_TypeDef *pMail_msg;
	uint8_t i = 0;
	
	osDelay(30);	/**< 延时,确保MwriteTask和MreadTask都已经运行 */
	
	/* 发送邮箱的指针到消息队列中 */
	for (i=0; i<MAILBOX_LEN*2; i++) {
		osMessageQueueGet(MailboxFreeQHandle, &pMail_msg, NULL, osWaitForever);	
		pMail_msg->var3 = i;
		osMessageQueuePut(MailboxQ01Handle, &pMail_msg, 0, 0);
		printf("[%d].\r\n", pMail_msg->var3);
		osDelay(30);
	}
	osDelay(100);
	printf("MwriteTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the MreadTask thread.
  * @param  argument: Not used
  * @retval None
  */
void MreadTask(void *argument)
{
	Mail_TypeDef *pMail_msg;
	uint8_t cnt = 0;
	
	/* 从消息队列中获取邮箱的指针,然后读取邮件内容 */
	for(cnt=0; cnt<MAILBOX_LEN*2; cnt++) {
		osMessageQueueGet(MailboxQ01Handle, &pMail_msg, NULL, osWaitForever);	
		printf("%d.\r\n", pMail_msg->var3);
		osMessageQueuePut(MailboxFreeQHandle, &pMail_msg, 0, 0);
		osDelay(100);
	}
	osDelay(100);
	printf("MreadTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数,创建两个任务,一个发送邮件到队列,一个从队列中获取消息。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int mailboxCmdFunc(int argc, char*argv[])
{
	/* Create the queue(s) */
	MailboxQ01Handle = osMessageQueueNew (MAILBOX_LEN, sizeof(void *), &MailboxQ01_attributes);
	MailboxFreeQHandle = osMessageQueueNew (MAILBOX_LEN, sizeof(void *), &MailboxFreeQ_attributes);
	
	mailbox_init();
  
	/* Create the thread(s) */
	printf("create MwriteTask.\r\n");
	MwriteTaskHandle = osThreadNew(MwriteTask, NULL, &MwriteTask_attributes);
	
	printf("create MreadTask.\r\n");
	MreadTaskHandle = osThreadNew(MreadTask, NULL, &MreadTask_attributes);
	
	printf("mailboxCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd mailboxCmd = 
{
	.name = "mailbox",
	.help = "Mailbox Example.",
	.fun = mailboxCmdFunc
};




