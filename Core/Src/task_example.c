
#include "stdio.h"
#include "cmsis_os.h"
#include "tiny_shell.h"

/* Definitions for ATask */
osThreadId_t ATaskHandle;
const osThreadAttr_t ATask_attributes = {
  .name = "ATask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for BTask */
osThreadId_t BTaskHandle;
const osThreadAttr_t BTask_attributes = {
  .name = "BTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for CTask */
osThreadId_t CTaskHandle;
const osThreadAttr_t CTask_attributes = {
  .name = "CTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Definitions for BinSem03 */
osSemaphoreId_t BinSem03Handle;
const osSemaphoreAttr_t BinSem03_attributes = {
  .name = "BinSem03"
};

/**
  * @brief  Function implementing the ATask thread.
  * @param  argument: Not used
  * @retval None
  */
void ATask(void *argument)
{
	uint8_t i;	
	
	osDelay(100);
	
	for (i=0; i<5; i++) {
		printf("ATask is runing.\r\n");
		osDelay(500);
	}
	osDelay(100);
	printf("ATask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  Function implementing the BTask thread.
  * @param  argument: Not used
  * @retval None
  */
void BTask(void *argument)
{	
	osDelay(200);
	
	printf("BTask is runing 1.\r\n");
	osDelay(300);
	osSemaphoreRelease(BinSem03Handle);
	printf("BTask is Suspend.\r\n");
	osThreadSuspend(BTaskHandle);
	
	printf("BTask is runing 2.\r\n");
	osDelay(300);
	osSemaphoreRelease(BinSem03Handle);
	printf("BTask is Suspend.\r\n");
	osThreadSuspend(BTaskHandle);
	
	while(1) {
		osDelay(300);
		printf("BTask is runing.\r\n");
	}
}

/**
  * @brief  Function implementing the CTask thread.
  * @param  argument: Not used
  * @retval None
  */
void CTask(void *argument)
{	
	osSemaphoreAcquire(BinSem03Handle, osWaitForever);
	osDelay(300);
	printf("Resume BTask 1.\r\n");
	osThreadResume(BTaskHandle);
	
	osSemaphoreAcquire(BinSem03Handle, osWaitForever);
	osDelay(300);
	printf("Resume BTask 2.\r\n");
	osThreadResume(BTaskHandle);
	
	osDelay(2000);
	printf("Terminate BTask.\r\n");
	osThreadTerminate(BTaskHandle);
	
	osDelay(100);
	printf("CTask exit.\r\n");
	osThreadExit();
}

/**
  * @brief  命令函数。
  * @param  [in] argc 输入的argv[]参数个数。
  * @param  [in] argv[] 参数列表。
  * @retval 0.
  */
int taskCmdFunc(int argc, char*argv[])
{
	/* Create the semaphores(s) */
	BinSem03Handle = osSemaphoreNew(1, 0, &BinSem03_attributes);
  
	/* Create the thread(s) */
	printf("create ATask.\r\n");
	ATaskHandle = osThreadNew(ATask, NULL, &ATask_attributes);	
	printf("create BTask.\r\n");
	BTaskHandle = osThreadNew(BTask, NULL, &BTask_attributes);
	printf("create CTask.\r\n");
	CTaskHandle = osThreadNew(CTask, NULL, &CTask_attributes);
	
	printf("taskCmdFunc exit.\r\n");
	
	return 0;
}

/* 命令描述 */
T_ShellCmd taskCmd = 
{
	.name = "task",
	.help = "Task Example.",
	.fun = taskCmdFunc
};



